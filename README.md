# How to add a publication


## Adding a paper published in proceedings (conference/workshop)

Edit [`_data/publications.yaml`](https://gitlab.com/umb-svl/umb-svl.gitlab.io/-/edit/master/_data/publications.yaml?ref_type=heads).

```yaml
- type: conference
  id: shelley-infer:verdi23
  authors:
  - Carlos Mão de Ferro
  - Tiago Cogumbreiro
  - Francisco Martins
  title: Formalizing Model Inference of MicroPython
  booktitle: VERDI
  year: 2023
  publisher: IEEE
  doi: 10.1109/DSN-W58399.2023.00069
  url: assets/shellypy-verdi23.pdf
  media:
  - title: Artifact
    url: https://zenodo.org/records/7892202
  funding: fearless22
```

1. `type: conference` declares this as a conference entry
2. `id:` ensure identifier is unique; look at examples for inspiration in naming convention
3. `authors:` encoded as a list
4. `title:` the title of the paper. This is valid HTML
5. `booktitle:` the acronym of the conference. The meta-data associated with
   the name of the conference is kept in sure the entry is available
   in: `_data/conferences.yaml`
6. `year:` of publication (as appears in DOI)
7. `publisher:` check other entries for consistency
8. `doi:` the DOI of the publication
9. `url:` the local copy of the article, stored in the `assets/` directory
10. `media:` use this add links to slides/artifacts/videos. Each entry
   has a `title:` and a `url:`
11. `funding:` ask PI for help on what to use

## Adding a journal

Edit [`_data/publications.yaml`](https://gitlab.com/umb-svl/umb-svl.gitlab.io/-/edit/master/_data/publications.yaml?ref_type=heads).

Journal articles look like this:
```yaml
- type: journal
  id: faial-drf:fmsd23
  authors: [Tiago Cogumbreiro, Julien Lange,  Dennis Liew, Hannah Zicarelli]
  title: "Memory Access Protocols: Certified Data-Race Freedom for GPU Kernels"
  journal: FMSD
  year: 2023
  doi: 10.1007/s10703-023-00415-0
  url: assets/faial-fmsd23.pdf
  media:
  - title: Slides
    url: assets/bu-sept-23.pdf
  extra: <a class="tag is-medium is-warning" href="https://link.springer.com/journal/10.1007/s10703-024-00459-w"><strong>invited paper (CAV'21 special issue)</strong></a>
  funding: fearless22
```

1. `type:` declares that this is a journal entry
2. `authors:` in this example use we list all the authors in the same line (see YAML tutorials for more info).
3. `journal:` entry specifies the journal acronym. See `_data/conferences.yaml` for how to edit each journal metadata.
4. `year:` of publication (as appears in DOI)
5. `doi:` the DOI of the publication
6. `url:` a local copy of the article, stored in the `assets/` directory
7. `media:` in this example we stored a copy of the slides
8. `extra:` Add any extra HTML to be included. Only used exceptionally.
9. `funding:` ask PI for help on what to use.

## Adding an unpublished article

Edit [`_data/publications.yaml`](https://gitlab.com/umb-svl/umb-svl.gitlab.io/-/edit/master/_data/publications.yaml?ref_type=heads).

```yaml
- type: unpublished
  id: regularc:tfpie24
  authors:  [Soroush Aghajani, Emma Kelminson, Tiago Cogumbreiro]
  title: "RegularIMP: an imperative calculus to describe regular languages (Extended Abstract)"
  year: 2024
  booktitle: TFPIE
  url: assets/regular-c-tfpie24.pdf
  funding: fearless22
```

1. `type:` declares that this is an unpublished entry. Any publication that does not appear
   in proceedings or in a journal, such as an informal (even if peer reviewed) extended abstract.
2. `booktitle:` declares the name of the event. See `_data/conferences.yaml` for how to add
   metadata to an event.

## Adding conference/unpublished metadata

Edit [`_data/conferences.yaml`](https://gitlab.com/umb-svl/umb-svl.gitlab.io/-/edit/master/_data/conferences.yaml?ref_type=heads).

```yaml
COORDINATION:
  title: International Conference on Coordination Models and Languages
  url:
    2013: https://link.springer.com/book/10.1007/978-3-642-38493-6
    2023: https://link.springer.com/book/10.1007/978-3-031-35361-1
```
1. `COORDINATION` the unique identifier of the conference
2. `title:` the name as it appears in the conference website
3. `url:` holds the proceedings for each year
4. `2023:` the key is the year, the value is the URL of the proceedings

### Adding journal metadata

Edit [`_data/conferences.yaml`](https://gitlab.com/umb-svl/umb-svl.gitlab.io/-/edit/master/_data/conferences.yaml?ref_type=heads).

```yaml
PACMPL:
  title: Proceedings of the ACM on Programming Languages
  url: https://dl.acm.org/journal/pacmpl
```
1. `title:` the full name of the journal.
2. `url:` the url of journal's website.
