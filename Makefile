all: build

serve:
	docker-compose run -p 4000:4000 jekyll

build:
	make -C _bibliography

clean:
	make -C _bibliography clean
	rm -rf _site/

